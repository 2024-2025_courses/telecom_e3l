def egcd(a, b):
    if b == 0:
        return a, 1, 0
    else:
        g, x, y = egcd(b, a % b)
        print (a, b, (a//b), g, x, y)
        return (g, y, x - (a // b) * y)


p=3
q=11

n=p*q
phi = (p-1)*(q-1)
e=3

g,x,y=egcd(phi, e)

if (g==1):
    d=y%phi
    print "p=", p, ",", hex(p), "q=", q, ",", hex(q)
    print "n=", n, ",", hex(n), "phi=", phi, ",", hex(phi)
    print "Public key=", e, ",", hex(e), n, ",", hex(n) 
    print "Private key=", d, ",", hex(d), n, ",", hex(n)
else:
    print "e and phi are not relatively prime"
    print "e=", e, "phi=", phi


