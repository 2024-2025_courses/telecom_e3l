#pip install pycryptodomex
from Cryptodome.Cipher import AES
from Cryptodome.Util   import Counter


key = b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f'
iv  = b'\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01'
message = b'il fait beau a8h'

iv_int = int.from_bytes(iv, byteorder='big')
ctr = Counter.new(128, initial_value=iv_int)

obj = AES.new (key,  AES.MODE_CTR, counter=ctr)
crypt=obj.encrypt(message)
print (crypt.hex())
print (crypt)


iv_int = int.from_bytes(iv, byteorder='big')
ctr = Counter.new(128, initial_value=iv_int)
obj = AES.new (key, AES.MODE_CTR, counter=ctr)
clear=obj.decrypt(crypt)
print (clear.hex())
print (clear)
