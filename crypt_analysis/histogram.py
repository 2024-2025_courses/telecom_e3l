#!/usr/bin/python

import numpy

MINCHAR = 0
MAXCHAR = 255


def histogram(filename):
    nb_item = (MAXCHAR - MINCHAR) + 1

    histogram_array = numpy.zeros(nb_item)
    file_crypt = open(filename, 'rb')

    for line in file_crypt:
        i = 0
        length = len(line)
        while i < length:
            val = line[i]
            if (val >= MINCHAR) and (val <= MAXCHAR):
                index = val - MINCHAR
                histogram_array[index] = histogram_array[index] + 1
            i = i + 1

    i = 0
    val = 0
    while i < nb_item:
        if histogram_array[i] != 0:
            if val > 0x20:
                print(hex(val), chr(val), histogram_array[i])
            else:
                print(hex(val), "    ", histogram_array[i])

        i = i + 1
        val = val + 1


if __name__ == '__main__':
    histogram('/workspace/work/methodology/2022_2023/ch7.bin')
