from digi.xbee.devices import XBeeDevice

def main():
    print(" +--------------------------------------+")
    print(" | XBee Python Library Send broadcast   |")
    print(" +--------------------------------------+\n")

    device = XBeeDevice("/dev/tty.usbserial-DN040BLV", 9600)
    device.open()
    device.send_data_broadcast("Hello XBee World!")
    print ("network", device.get_network())
    print ("protocol", device.get_protocol())
    print ("16bit addr", device.get_16bit_addr())
    print ("64bit addr", device.get_64bit_addr())
    print ("Operating PAN ID", device.get_pan_id())


    device.close()


if __name__ == '__main__':
    main()
