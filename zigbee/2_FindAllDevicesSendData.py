# https://github.com/digidotcom/python-xbee/blob/master/doc/user_doc/discovering_the_xbee_network.rst


from digi.xbee.devices import XBeeDevice
import time

def main():
    print(" +------------------------------------------------------+")
    print(" | XBee Python Library: Find all devices and send data  |")
    print(" +------------------------------------------------------+\n")

    device = XBeeDevice("/dev/tty.usbserial-DN040BLV", 9600)
    device.open()
    
    # Get the XBee Network object from the XBee device.
    xbee_network = device.get_network()

    # Start the discovery process and wait for it to be over.
    xbee_network.start_discovery_process()
    while xbee_network.is_discovery_running():
        time.sleep(0.5)

    # Get a list of the devices added to the network.
    remote_devices = xbee_network.get_devices()
    print ("nb remote devices", xbee_network.get_number_devices())

    for x in remote_devices:
       print ("device", x)
       device.send_data(x, "Hello world")

    device.close()


if __name__ == '__main__':
    main()
